# Nomenclatura

- [Imports](#Imports)
- [Constantes](#Constantes)
- [Configuracion](#Configuracion)

## Imports

```js
import cdk = require('@aws-cdk/core');
import sqs = require('@aws-cdk/aws-sqs');
import codecommit = require('@aws-cdk/aws-codecommit');
import iam = require('@aws-cdk/aws-iam');
import lambda = require('@aws-cdk/aws-lambda');
import stepfunction = require('@aws-cdk/aws-stepfunctions');
import event = require('@aws-cdk/aws-events');
import codebuild = require ('@aws-cdk/aws-codebuild');
import sns = require ('@aws-cdk/aws-sns');
```

## Constantes

### Clase principal `Constants`

- [Descripción de cada constante](./lib/constants.ts)

```ts
IDProject: string;
EnvProject: Environment;
OptionQueue: QueueOption;
BucketCFTemplates: string;
PipelineName: string;
RepositoryName: string;
BucketPipelineDataName: string;
ProjectRepositoryName: string;
Queue: string;
BucketArtifactName: string;
KeyBucket: string;
IDCFront: string;
BucketContentName: string;
BucketPipelineData: string;
EmailFromApproval: string;
EmailTopproval: string;
FWProjectRepositoryName: string;
```

### Enums

```ts
enum Environment {
  INT = 'int',
  PRE = 'pre',
  PRO = 'pro'
}

enum QueueOption {
  S3 = 's3',
  CODECOMMIT = 'CreateCodeCommit'
}
```

## Configuracion

### Interfaz principal `ActionConfig`

```ts
export default interface ActionConfig {
  Name: string;
  ActionTypeId: {
    Category: Category;
    Owner: Owner;
    Provider: Provider;
    Version: number;
  };
  InputArtifacts: {
    Name: string;
  };
  OutputArtifacts: {
    Name: string;
  };
  RunOrder: number;
  Configuration: ConfigS3 | ConfigCodeCommit | ConfigCloudFormation | ConfigCodeBuild | ConfigCodeDeploy | ConfigManualApproval | ConfigLambda;
}
```

* [Pipeline:](https://docs.aws.amazon.com/es_es/AWSCloudFormation/latest/UserGuide/aws-resource-codepipeline-pipeline.html)

  * [[{Stages}]:](https://docs.aws.amazon.com/es_es/AWSCloudFormation/latest/UserGuide/aws-properties-codepipeline-pipeline-stages.html)

    * [[{Actions}]](https://docs.aws.amazon.com/es_es/AWSCloudFormation/latest/UserGuide/aws-properties-codepipeline-pipeline-stages-actions.html#cfn-codepipeline-pipeline-stages-actions-configuration)

### Enums

```ts
enum Category {
  SOURCE = 'Source',
  BUILD = 'Build',
  DEPLOY = 'Deploy',
  INVOKE = 'Invoke',
  APPROVAL = 'Approval'
}

enum Provider {
  S3 = 'S3',
  CODEBUILD = 'CodeBuild',
  CODEDEPLOY = 'CodeDeploy',
  LAMBDA = 'Lambda',
  MANUAL = 'Manual'
}

enum Owner {
  AWS = 'AWS'
}
```

### Interfaces

```ts
interface ConfigS3 {
  S3Bucket: string;
  S3ObjectKey: string;
}

interface ConfigCodeCommit {
  RepositoryName: string;
  BranchName: string;
}

interface ConfigCloudFormation {
  ActionMode: string;
  StackName: string;
}

interface ConfigCodeBuild {
  ProjectName: string;
}

interface ConfigCodeDeploy {
  ApplicationName: string;
  DeploymentGroupName: string;
}

interface ConfigManualApproval {
  NotificationArn: string;
  CustomData: string;
}

interface ConfigLambda {
  FunctionName: string;
}
```
