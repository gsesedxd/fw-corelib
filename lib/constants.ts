/**
 *  Evironment (INT | PRE | PRO)
 * @return string
 */
enum Environment {
  INT = 'int',
  PRE = 'pre',
  PRO = 'pro'
}

/**
 *  QueueOption (S3 | CODECOMMIT)
 * @return string
 */
enum QueueOption {
  S3 = 's3',
  CODECOMMIT = 'CreateCodeCommit'
}

export class Constants {
  /**
   * **ID** of the project.
   * @type string
   */
  public static readonly IDProject: string = 'foo';

  /**
   * **ENVIRONMENT** of the project.
   * @type Environment (Enum)
   */
  public static readonly EnvProject: Environment = Environment.PRO;

  /**
   * Type of **SOURCE** incoming to the Queue.
   * @type QueueOption (Enum)
   */
  public static readonly OptionQueue: QueueOption = QueueOption.CODECOMMIT;

  /**
   * **BUCKET** name where this template is located.
   * @type string
   */
  public static readonly BucketCFTemplates: string = 'tempalate-name';

  /**
   * **NAME** of the pipeline.
   * @type string
   */
  public static readonly PipelineName: string = 'fw-pipeline';

  /**
   * Client/Project repository name.
   * @type string
   */
  public static readonly RepositoryName: string = 'repository-tests';

  /**
   * **BUCKET NAME** where the data of pipeline is located.
   * @type string
   */
  public static readonly BucketPipelineDataName: string = 'bucket-pipeline';

  /**
   * Name of repository bitbucket where the project is stored.
   * @type string
   */
  public static readonly ProjectRepositoryName: string = 'project-repo-name';

  /**
   * **URL** of the queue.
   * @type string
   */
  public static readonly Queue: string = 'queue';

  /**
   * **BUCKET** that contain pipeline artifacts.
   * @type string
   */
  public static readonly BucketArtifactName: string = 'bucket-artifact';

  /**
   * **NAME** of de KeyBucket.
   * @type string
   */
  public static readonly KeyBucket: string = 'bucket';

  /**
   * **ID** of the CloudFront distribution.
   * @type string
   */
  public static readonly IDCFront: string = 'cf-bar-id';

  /**
   * **BUCKET** that contain the WebApp.
   * @type string
   */
  public static readonly BucketContentName: string = 'bucket-content-name';

  /**
   * **BUCKET** name where the data of pipeline is located.
   * @type string
   */
  public static readonly BucketPipelineData: string = 'bucket-content-data';

  /**
   * **EMAIL** sender. (**FROM**)
   * @type string
   */
  public static readonly EmailFromApproval: string = 'insertvalue@example.com';

  /**
   * **EMAIL** reciever. (**TO**)
   * @type string
   */
  public static readonly EmailToApproval: string = 'insertvalue@example.com';

  /**
   * **REPOSITORY** name where the files will be copied from.
   * @default 'fwmovilidad'
   * @type string
   */
  public static readonly FWProjectRepositoryName: string = 'fwmovilidad';
}