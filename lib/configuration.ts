export enum Category {
  SOURCE = 'Source',
  BUILD = 'Build',
  DEPLOY = 'Deploy',
  INVOKE = 'Invoke',
  APPROVAL = 'Approval'
}

export enum Provider {
  S3 = 'S3',
  CODEBUILD = 'CodeBuild',
  CODEDEPLOY = 'CodeDeploy',
  LAMBDA = 'Lambda',
  MANUAL = 'Manual'
}

export enum Owner {
  AWS = 'AWS'
}


// Custom configs
interface ConfigS3 {
  S3Bucket: string;
  S3ObjectKey: string;
}

interface ConfigCodeCommit {
  RepositoryName: string;
  BranchName: string;
}

interface ConfigCloudFormation {
  ActionMode: string;
  StackName: string;
}

interface ConfigCodeBuild {
  ProjectName: string;
}

interface ConfigCodeDeploy {
  ApplicationName: string;
  DeploymentGroupName: string;
}

interface ConfigManualApproval {
  NotificationArn: string;
  CustomData: string;
}

interface ConfigLambda {
  FunctionName: string;
}

// Action Config
export interface ActionConfig {
  Name: string;
  ActionTypeId: {
    Category: Category;
    Owner: Owner;
    Provider: Provider;
    Version: number;
  };
  InputArtifacts: {
    Name: string;
  };
  OutputArtifacts: {
    Name: string;
  };
  RunOrder: number;
  Configuration: ConfigS3 | ConfigCodeCommit | ConfigCloudFormation | ConfigCodeBuild | ConfigCodeDeploy | ConfigManualApproval | ConfigLambda;
}
